#include <string>
#include <random>
#include <iostream>
using namespace std;

string randDNA(int seed, string bases, int n)
{
std::mt19937 eng1(seed);

int min = 0;
int max = bases.size() - 1;
string DNA; 
std::uniform_int_distribution<int> uniform(min,max);
for(int i=0 ; i < n;  i++)
{
int num = uniform(eng1);
DNA = DNA + bases[num];
}
return DNA;
}
